# ISISfishR

The goal of ISISfishR is to provide functions to handle and manipulate data for ISISfish.

Installation
You can install the development version of ISISfishR like using the remotes package

install.packages("remotes")
library(remotes)
remotes::install_gitlab(repo = "isis-fish-tools/ISISfishR",
                        host = "https://gitlab.ifremer.fr")
